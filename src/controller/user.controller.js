import { response } from "express";
import user from "../model/user";
import User from "../model/user";

const handleErr = (err, res) => {
  res.status(500).json({
    status: res.statusCode,
    message: err.message,
  });
  console.log(err);
};

export default {
  //testing
  test: (req, res) => {
    res.json({
      message: "Api working...",
    });
  },
  //creating new user
  createUser: async (req, res) => {
    const data = req.body;
    //checking whether the user exists or not
    const { public_address } = data;
    const users = await User.find({ public_address });
    if (users[0]) {
      return res.status(409).json({
        status: res.statusCode,
        message: "user already exists !!",
        data: [],
      });
    }
    //creating new user
    await User.create(data)
      .then((response) =>
        res.status(200).json({
          status: res.statusCode,
          message: "Successfully created !!",
          data: response,
        })
      )
      .catch((err) => handleErr(err, res));
  },
  //for displaying all user
  displayAll: async (req, res) => {
    await User.find({})
      .then((response) => {
        res.status(200).json({
          status: res.statusCode,
          message: "All Users",
          data: response,
        });
      })
      .catch((err) => handleErr(err, res));
  },
  //display a single user
  displaySingle: async (req, res) => {
    const { publicAddress } = req.params;
    await User.find({ public_address: publicAddress })
      .then((response) => {
        if (response.length === 0) {
          return res.status(404).json({
            status: res.statusCode,
            message: "No user exist !!",
            data: [],
          });
        }
        res.status(200).json({
          status: res.statusCode,
          message: "required user",
          data: response[0],
        });
      })
      .catch((err) => handleErr(err, res));
  },
  //updating a user
  updateUser: async (req, res) => {
    const { userId } = req.params;
    const data = req.body;
    const oldValue = await User.findById(userId);
    const updatedData = {
      name: data.name ? data.name : oldValue.name,
      //changing of public address not allowed
      public_address: oldValue.public_address,
      age: data.age ? data.age : oldValue.age,
      reputation_score: data.reputation_score
        ? data.reputation_score
        : oldValue.reputation_score,
    };
    await User.findByIdAndUpdate(userId, updatedData)
      .then((response) => {
        res.status(200).json({
          status: res.statusCode,
          message: "Update successfully !!",
          data: [],
        });
      })
      .catch((err) => handleErr(err, res));
  },
  //deleting a user
  deleteUser: async (req, res) => {
    const { userId } = req.params;
    await User.findByIdAndRemove(userId)
      .then((response) => {
        if (!response) {
          return res.status(404).json({
            status: res.statusCode,
            message: "User not found !!",
            data: [],
          });
        }
        res.status(200).json({
          status: res.statusCode,
          message: "Deleted successfully !!",
          data: [],
        });
      })
      .catch((err) => handleErr(err, res));
  },
};
