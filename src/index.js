import express from "express";
import cors from "cors";
import fs from "fs";
const PORT = 5000;
const app = express();

//connecting database
require("./config/db");
//allowing cors
app.use(cors());
//allowing data parsing
app.use(express.json());

//adding all routes files
fs.readdirSync("./src/route").forEach((file) => {
  if (~file.indexOf(".js")) {
    require("./route/" + file)(app);
  }
});

app.listen(PORT, () => console.log(`Server is started on ${PORT}`));
