import mongoose from "mongoose";

const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  public_address: {
    type: String,
    required: true,
    unique: true,
  },
  age: Number,
  reputation_score: Number,
});

export default mongoose.model("users", userSchema);
