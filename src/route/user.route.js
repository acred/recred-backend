import express from "express";
import userController from "../controller/user.controller";

const route = express.Router();

module.exports = (app) => {
  //for testing
  route.get("/test", userController.test);
  //creating user
  route.post("/create", userController.createUser);
  //reading all user
  route.get("/", userController.displayAll);
  //getting a single user
  route.get("/:publicAddress", userController.displaySingle);
  //updating a user
  route.put("/:userId", userController.updateUser);
  //deleting a user
  route.delete("/:userId", userController.deleteUser);

  app.use("/api/user", route);
};
